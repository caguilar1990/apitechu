package com.techu.apitechuv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class UserModel {

    @Id
    String id;
    int age;
    String name;

    public String getId() {
        return id;
    }

    public UserModel() {
    }

    public UserModel(String id, int age, String name) {
        this.id = id;
        this.age = age;
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
