package com.techu.apitechuv2.services;


import com.techu.apitechuv2.models.ProductoModel;
import com.techu.apitechuv2.repositories.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    /// metodos que vamos a invocar desde el controlador.

    @Autowired
    ProductoRepository productoRepository;

    // Read
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    // Read by Id
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }

    public boolean existsById(String id) {
        return productoRepository.existsById(id);
    }

    // Create
    public ProductoModel save(ProductoModel newProduct){
        return productoRepository.save(newProduct);
    }

    // Delete
    public boolean isDeleted(ProductoModel deletedProduct){
        try {
            productoRepository.delete(deletedProduct);
            return true;
        } catch (Exception e){
            return false;
        }
    }

}

