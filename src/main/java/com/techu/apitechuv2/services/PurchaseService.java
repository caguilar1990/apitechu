package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.PurchaseModel;
import com.techu.apitechuv2.repositories.ProductoRepository;
import com.techu.apitechuv2.repositories.PurchaseRepository;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductoRepository productoRepository;

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase -- Service");

        PurchaseServiceResponse result = new PurchaseServiceResponse();
        result.setPurchaseModel(purchase);

        if (this.userRepository.findById(purchase.getUserId()).isEmpty() == true) {

            result.setMsg("El usuario de la compra no se encuentra");
            result.setHttpsStatus(HttpStatus.BAD_REQUEST);
            return result;

        }

        if (this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {

            result.setMsg("Ya hay una compra con esa Id");
            result.setHttpsStatus(HttpStatus.BAD_REQUEST);
            return result;

        }
        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItems : purchase.getPurchaseItems().entrySet()) {
            if (this.productoRepository.findById(purchaseItems.getKey()).isEmpty()) {
                result.setMsg("el producto con la id " + purchaseItems.getKey() + "no existe");
                result.setHttpsStatus(HttpStatus.BAD_REQUEST);
                return result;
            } else {
                amount += this.productoRepository.findById(purchaseItems.getKey()).get().getPrice() * purchaseItems.getValue();



            }

        }
        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setHttpsStatus(HttpStatus.CREATED);
        return result;
    }
}
