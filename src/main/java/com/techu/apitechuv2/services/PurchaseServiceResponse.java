package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.PurchaseModel;
import org.springframework.http.HttpStatus;

//actuar de puente entre el servicio y el controlador

public class PurchaseServiceResponse {

    private String msg;
    private PurchaseModel purchaseModel;
    private HttpStatus httpsStatus;

    public PurchaseServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public PurchaseModel getPurchaseModel() {
        return purchaseModel;
    }

    public HttpStatus getHttpsStatus() {
        return httpsStatus;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setPurchaseModel(PurchaseModel purchaseModel) {
        this.purchaseModel = purchaseModel;
    }

    public void setHttpsStatus(HttpStatus httpsStatus) {
        this.httpsStatus = httpsStatus;
    }
}
