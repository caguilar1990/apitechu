package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {


    @Autowired
    UserRepository userRepository;


    //CRUD Functions

    //obtener un listado de todos los usuarios
    public List<UserModel> findAll(String orderby) {

        List<UserModel> result;
        if (orderby != null) {
            result = this.userRepository.findAll(Sort.by(orderby));
        } else {
            result = this.userRepository.findAll();
        }

        return result;

    }

    //obtener usuario por su Id

    public Optional<UserModel> findById(String id) {
        return userRepository.findById(id);
    }


    // crear un usuario
    public UserModel addUser(UserModel userModel) {
        return this.userRepository.save(userModel);

    }


    public boolean isDeleted(UserModel deletedUser) {
        try {
            userRepository.delete(deletedUser);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean existsById(String id) {
        return userRepository.existsById(id);
    }


}
