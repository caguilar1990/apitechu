package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.PurchaseModel;
import com.techu.apitechuv2.services.ProductoService;
import com.techu.apitechuv2.services.PurchaseService;
import com.techu.apitechuv2.services.PurchaseServiceResponse;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;
    @Autowired
    UserService userService;
    @Autowired
    ProductoService productService;

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchaseModel){
        System.out.println("addPurchase");
        System.out.println("La id de la compra es :"+ purchaseModel.getId());
        System.out.println("La id del usuario de la compra es : "+ purchaseModel.getUserId());
        System.out.println("La lista de la compra es : "+ purchaseModel.getPurchaseItems());
        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchaseModel);
        return new ResponseEntity<>(result, result.getHttpsStatus());

    }
}
