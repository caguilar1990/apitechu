package com.techu.apitechuv2.controllers;


import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2/")
public class UserController {


    @Autowired
    UserService userService;

//listado de user ordenados
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name = "$orderby", required = false) String orderBy) {

        System.out.println("getUsers");

        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }
// recuperacion user by id

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    //añadir
    @PostMapping("/users")
    @ResponseBody
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel) {
        System.out.println("---------------------- addUser ---------------------");
        System.out.println("La id del usuario es " + userModel.getId());
        System.out.println("El nombre del usuario " + userModel.getName());
        System.out.println("La edad del usuario es " + userModel.getAge());

        return new ResponseEntity<>(this.userService.addUser(userModel), HttpStatus.CREATED);


    }

    //actualizar
    @PutMapping("/users/{id}")
    public void putUser(@RequestBody UserModel updatedUser, @PathVariable String id) {
        if (userService.existsById(id)) {
            userService.addUser(updatedUser);
        }
    }

    //Borrado
    @DeleteMapping("/users/{id}")
    public boolean deleteUsert(@PathVariable String id) {//
        if (userService.existsById(id)) {
            UserModel userTODelete = userService.findById(id).get();
            userService.isDeleted(userTODelete);
            return true;
        } else
            return false;
    }


}

