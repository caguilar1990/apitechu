

package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductoModel;
import com.techu.apitechuv2.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/products")
    public List<ProductoModel> getProducts(){
        return productoService.findAll();
    }

    @GetMapping("/products/{id}")
    public Optional<ProductoModel> getProductById(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/products")
    public ProductoModel postProduct(@RequestBody ProductoModel newProduct){
        try {
            return productoService.save(newProduct);
        } catch (Exception e){
            return null;
        }
    }

    @PutMapping("/products/{id}")
    public void putProduct(@RequestBody ProductoModel updatedProduct, @PathVariable String id){
        if (productoService.existsById(id)) {
            productoService.save(updatedProduct);
        }
    }

    @DeleteMapping("/products/{id}")
    public boolean deleteProduct(@PathVariable String id){//, @RequestBody ProductoModel productToDelete) {
        if (productoService.existsById(id)) {
            ProductoModel productToDelete = productoService.findById(id).get();
            productoService.isDeleted(productToDelete);
            return true;
        } else
            return false;
    }

    @PatchMapping("/products/{id}")
    public ProductoModel patchProduct(@PathVariable String id, @RequestBody ProductoModel patchedProduct) {
        if(productoService.findById(id).isPresent() == true) {
            ProductoModel productToPatch = productoService.findById(id).get();
            if(patchedProduct.getPrice() != null) {
                productToPatch.setPrice(patchedProduct.getPrice());
            }
            if (patchedProduct.getDescription() != null) {
                productToPatch.setDescription(patchedProduct.getDescription());
            }
            return productoService.save(productToPatch);
        }
        return null;
    }

}