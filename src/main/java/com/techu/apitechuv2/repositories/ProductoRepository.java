package com.techu.apitechuv2.repositories;

import com.techu.apitechuv2.models.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {
}
