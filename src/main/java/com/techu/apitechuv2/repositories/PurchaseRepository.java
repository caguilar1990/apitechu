package com.techu.apitechuv2.repositories;

import com.techu.apitechuv2.models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends MongoRepository<PurchaseModel,String> {
}
