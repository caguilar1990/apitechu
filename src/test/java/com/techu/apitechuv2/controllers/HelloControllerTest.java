package com.techu.apitechuv2.controllers;

import org.junit.Assert;
import org.junit.Test;


public class HelloControllerTest {
    @Test
    public void HelloTest(){
        HelloController sut = new HelloController();
        Assert.assertEquals("Hola Carlos!",sut.hello("Carlos"));
    }
}
